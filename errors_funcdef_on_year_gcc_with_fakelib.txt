Using func_defs.py with

    ast = parse_file(filename, use_cpp=True,
            cpp_path='gcc',
            cpp_args=['-E', r'-I../utils/fake_libc_include'])

Executes properly.

viktor@ubuntu:~/tools/pycparser-master/examples$ python func_defs.py c_files/year2.c
convert at c_files/year2.c:10
main at c_files/year2.c:36

No output whatsoever.

viktor@ubuntu:~/tools/pycparser-master/examples$ python func_defs.py c_files/year2.i

